import streamlit as st
import pandas as pd
from PIL import Image
from io import BytesIO
import requests
import warnings

warnings.filterwarnings("ignore")

# Устанавливаем размер кнопок в пикселях
st.set_option('deprecation.showPyplotGlobalUse', False)
st.set_option('deprecation.showfileUploaderEncoding', False)
st.set_option('deprecation.showPyplotGlobalUse', False)
st.set_option('deprecation.showfileUploaderEncoding', False)
st.set_option('deprecation.showfileUploaderEncoding', False)
# st.set_page_config(page_title="Image Annotation Tool", button_size=30)

# загрузка данных из файла
@st.cache(allow_output_mutation=True)
def load_data(file):
    return pd.read_csv(file)

@st.cache(hash_funcs={BytesIO: lambda _: None})
def load_image(image_url):
    return Image.open(BytesIO(requests.get(image_url).content))

# отображение изображения и выбор класса
def annotate_image(image_path, default_class, image_counter, col):
    col.write(f"Картинка {image_counter + 1}")
    image = load_image(image_path)
    # Показываем картинку с шириной колонки
    col.image(image, use_column_width=True)

    # выбор класса
    class_options = ['0', '1', '2']
    class_index = class_options.index(str(default_class))
    # Изменяем размер радиокнопок на 30 пикселей
    class_select = col.radio('Выберите класс:', class_options, index=class_index, key=f"class_select_{image_counter}")

    return int(class_select)

def save_data(data):
    data.to_csv('urls_label.csv', index=False)

def main():
    st.title("Image Annotation Tool")

    file = st.file_uploader("Загрузите CSV файл", type=["csv"])
    if file is not None:
        data = load_data(file)
        image_urls = data['url'].tolist()
        classes = data['class'].tolist()

        start_index = st.number_input("Введите номер картинки, с которой начать выбор", min_value=1, max_value=len(image_urls), value=1)

        # вывод изображений
        for i in range(start_index-1, len(image_urls), 2):
            col1, col2 = st.columns(2)
            if i < len(image_urls) - 1:
                class_index1 = annotate_image(image_urls[i], classes[i], i, col1)
                class_index2 = annotate_image(image_urls[i+1], classes[i+1], i+1, col2)
            else:
                class_index1 = annotate_image(image_urls[i], classes[i], i, col1)
                class_index2 = None

            if class_index1 != classes[i]:
                data.at[i, 'class'] = class_index1
                save_data(data)
                st.write(f"Изменено значение класса изображения {i+1} на {class_index1}.")

            if class_index2 is not None and class_index2 != classes[i+1]:
                data.at[i+1, 'class'] = class_index2
                save_data(data)
                st.write(f"Изменено значение класса изображения {i+2} на {class_index2}.")

        st.write('Вы разметили все изображения!')


if __name__ == '__main__':
    main()
